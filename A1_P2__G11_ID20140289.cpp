#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
class BigDecimalInt
{
	string s;
	bool ve;
public:
	BigDecimalInt ();
	void set_s(string);
	void set_ve(bool);
	string get_s();
	bool get_ve();
	BigDecimalInt (string decStr);
	BigDecimalInt (int decInt);
	BigDecimalInt operator+ (BigDecimalInt anotherDec);
	BigDecimalInt operator- (BigDecimalInt anotherDec);
	BigDecimalInt operator= (BigDecimalInt anotherDec);
	BigDecimalInt operator= (string anotherDec);
	int size();
	friend ostream& operator << (ostream& out, BigDecimalInt b);
	~BigDecimalInt(){}
};

void BigDecimalInt::set_s(string s1)
{
	s=s1;
}
void BigDecimalInt::set_ve(bool k)
{
	ve=k;
}
	string BigDecimalInt::get_s()
	{
		return s;
	}
	bool BigDecimalInt::get_ve()
	{
		return ve;
	}
BigDecimalInt::BigDecimalInt ()
	{
		s="";
		ve=0;
	}
	/*
	BigDecimalInt::BigDecimalInt (const BigDecimalInt& x);
	{
		s=x.s;
		ve=x.ve;
	}
	*/
BigDecimalInt::BigDecimalInt (string decStr)
{
	bool w=true;
	if(decStr.size()>0&&(decStr[0]<'0'||decStr[0]>'9')&&decStr[0]!='-')
	{
		cout<<"error\n";
		w=false;
	}
	for (int i=1;i< decStr.size();++i)
	{
		if(decStr[i]<'0'||decStr[i]>'9')
		{
			cout<<"error\n";
			w=false;
			break;
		}
	}
	if(w)
	{
		if(decStr.size()>0&&decStr[0]=='-')
		{
			ve=1;
			s=decStr;
			s.erase(0,1);
		}
		else
		{
			ve=0;
			s=decStr;
		}
	}
}

BigDecimalInt::BigDecimalInt (int decInt)
{
	s="";
	if (decInt<0)
	{
		ve=1;
		decInt*=-1;
	}
	else
	{
		ve=0;
	}
	while(decInt>0)
	{
		string x="0";
		x[0]=((decInt%10)+'0');
		s=x+s;
		decInt/=10;
	}
}
BigDecimalInt BigDecimalInt:: operator+ (BigDecimalInt anotherDec)
{
	BigDecimalInt res;
	res.s="";
	if(ve==anotherDec.ve)
	{
		res.ve = ve;
	}
	else
	{
		if(s!=anotherDec.s)
		{
			if(ve==1)
			{
				ve=0;
				res =anotherDec-(*this);
				ve=1;
			}
			else
			{
				anotherDec.ve=0;
				res =(*this)-anotherDec;
				anotherDec.ve=1;
			}
		}
		else
		{
			res.ve=0;
			res.s="0";
		}
		return res;
	}
	int carry=0;
	for (int i=s.size()-1,j=anotherDec.s.size()-1;i>=0||j>=0;--i,--j)
	{

		if(i>=0&&j>=0)
		{
			if((s[i]-'0')+(anotherDec.s[j]-'0')+carry<10)
			{
				string x="0";
				x[0]=(s[i]+anotherDec.s[j]+carry-'0');
				res.s=x+res.s;
				carry =0;
			}
			else
			{
				string x="0";
				x[0]=((((s[i]-'0')+(anotherDec.s[j]-'0')+carry)%10)+'0');
				res.s=x+res.s;
				carry=((s[i]-'0')+(anotherDec.s[j]-'0')+carry)/10;
			}

		}
		else if(i>=0&&j<0)
		{
			if((s[i]-'0')+carry<10)
			{
				string x="0";
				x[0]=(s[i]+carry);
				res.s=x+res.s;
				carry=0;
			}
			else
			{
				string x="0";
				x[0]=((((s[i]-'0')+carry)%10)+'0');
				res.s=x+res.s;
				carry=((s[i]-'0')+carry)/10;
			}
		}
		else if(j>=0&&i<0)
		{
			if((anotherDec.s[j]-'0')+carry<10)
			{
				string x="0";
				x[0]=(anotherDec.s[j]+carry);
				res.s=x+res.s;
				carry=0;
			}
			else
			{
				string x="0";
				x[0]=((((anotherDec.s[j]-'0')+carry)%10)+'0');
				res.s=(x)+res.s;
				carry=((anotherDec.s[j]-'0')+carry)/10;
			}
		}
	}
	if (carry > 0) {
		string b;
		b="0";
		b[0]=carry+'0';
		res.s=b+res.s;
	}
	return res;

}
BigDecimalInt BigDecimalInt::operator- (BigDecimalInt anotherDec)
{
	BigDecimalInt res;
	if(ve!=anotherDec.ve)
	{
		bool k= ve;
		ve=0;
		anotherDec.ve=0;
		res=(*this)+anotherDec;
		ve=k;
		res.ve=ve;
		anotherDec.ve= !k;
		return res;
	}
	else
	{
		if (s==anotherDec.s)
		{
			res.ve=0;
			res.s="0";
			return res;
		}
		res.s="";
		bool e;
		if(s.size()>anotherDec.s.size())
		{
			e=true;
		}
		else if (s.size()<anotherDec.s.size())
		{
			e=false;
		}
		else
		{
			for (int i=0;i<s.size();++i)
			{
				if (s[i]>anotherDec.s[i])
				{
					e=true;
					break;
				}
				else if (s[i]<anotherDec.s[i])
				{
					e=false;
					break;
				}
				else
				{
					continue;
				}

			}
		}
		if(e)
		{
			res.ve=ve;
			string t;
			t=s;
			for (int i=s.size()-1,j=anotherDec.s.size()-1;i>=0||j>=0;--i,--j)
			{
				if(i>=0&&j>=0)
				{
					if(s[i]>=anotherDec.s[j])
					{
						string x="0";
						x[0]=(s[i]-anotherDec.s[j]+'0') ;
						res.s=(x)+ res.s;
					}
					else
					{
						int m;
						if(i-1>=0)
						{
							m=i-1;
						}
						while(s[m]=='0')
						{
							s[m]='9';
							m--;
						}
						s[m]--;
						string x="0";
						x[0]=(10+s[i]-anotherDec.s[j]+'0');
						res.s=(x)+res.s;
					}
				}
				if(i>=0&&j<0)
				{
					res.s=s[i]+res.s;
				}
			}
			s=t;

		}
		else
		{
			res.ve=anotherDec.ve;
			string t;
			t=anotherDec.s;
			for (int i=s.size()-1,j=anotherDec.s.size()-1;i>=0||j>=0;--i,--j)
			{
				if(i>=0&&j>=0)
				{
					if(anotherDec.s[j]>=s[i])
					{
						string x="0";
						x[0]=(anotherDec.s[j]-s[i]+'0');
						res.s=(x) + res.s;
					}
					else
					{
						int m;
						if(j-1>=0)
						{
							m=j-1;
						}
						while(anotherDec.s[m]=='0')
						{
							anotherDec.s[m]='9';
							m--;
						}
						s[m]--;
						string x="0";
						x[0]=(10+anotherDec.s[j]-s[i]+'0');
						res.s=(x)+res.s;
					}
				}
				if(j>=0&&i<0)
				{
					res.s=anotherDec.s[j]+res.s;
				}
			}
			anotherDec.s=t;
		}
	}
	return res;
}
BigDecimalInt BigDecimalInt::operator= (BigDecimalInt anotherDec)
{
	s=anotherDec.s;
	ve=anotherDec.ve;
	return *this;
}
int BigDecimalInt::size()
{
	return s.size();
}
ostream& operator << (ostream& out, BigDecimalInt b)
{
	if(b.ve==1)
	{
		out<<"-";
	}
	out<<b.s;
	return out;
}


class BigReal {

	BigDecimalInt intPart, fractionPart;
	public:
	BigReal (string realNumber); // Should reject bad input
	BigReal (double realNumber);
	BigReal operator+ (BigReal other);
	BigReal operator= (BigReal other);
	friend ostream& operator << (ostream& out, BigReal num);
	~BigReal();
};
BigReal::BigReal (string realNumber) // Should reject bad input
	{
		int c=0;
		bool k1=true;
		for(int i=0;i<realNumber.size();++i)
		{
			if((realNumber[i]<'0'||realNumber[i]>'9')&&realNumber[i]!='.')
			{
				cout<<"error\n";
				k1=false;
				break;
			}
			if(realNumber[i]=='.')
			{
				c++;
				if(c>1)
						{
							cout<<"error\n";
							k1=false;
							break;
						}

			}
		}
		if(k1)
		{
		intPart.set_s("");
		fractionPart.set_s("");
		bool k=true;
		string x="0",y;
		for(int i=0;i<realNumber.size();++i)
		{
			if(realNumber[i]=='.')
			{
				k=false;
				continue ;
			}
			if(k)
			{
				x[0]=realNumber[i];
				y=intPart.get_s();
				y+= x;
				intPart.set_s(y);
			}
			if(!k)
			{
				x[0]=realNumber[i];
				y=fractionPart.get_s();
				y+=x;
				fractionPart.set_s(y);
			}
		}
		}
	}
	BigReal::BigReal (double realNumber = 0.0)
	{
		intPart.set_s("");
		fractionPart.set_s("");
		string b;
		int x=(int)realNumber;
		double y=realNumber-(double)x;
		while(x>0)
		{
			string z="0";
			z[0]=(x%10)+'0';
			b=intPart.get_s();
			b=z+b;
			intPart.set_s(b);
			x/=10;
		}
		while(y>0.0)
		{
			y*=10;
			string z="0";
			z[0]=((int)y)+'0';
			b=fractionPart.get_s();
			b=z+b;
			fractionPart.set_s(b);
			int a=(int)y;
			y=(double)y-a;
		}
	}
	BigReal BigReal::operator+ (BigReal other)
	{
		BigReal res;
		res.intPart= intPart + other.intPart;
		string x,y;
		x=fractionPart.get_s();
		y=other.fractionPart.get_s();
		res.fractionPart.set_s(x);
		if(y>x)
		{
			y=fractionPart.get_s();
			x=other.fractionPart.get_s();
			res.fractionPart.set_s(x);
		}
		int carry=0;
		for (int i=y.size()-1;i>=0;++i)
		{

			if((x[i]-'0')+(y[i]-'0')+carry<10)
			{
				res.fractionPart.get_s()[i]=(x[i]+y[i]+carry-'0');
				carry =0;
			}
			else
			{
				res.fractionPart.get_s()[i]=((((x[i]-'0')+(y[i]-'0')+carry)%10)+'0');
				carry=((x[i]-'0')+(y[i]-'0')+carry)/10;
			}
		}
		if (carry>0)
		{
			res.intPart = res.intPart + BigDecimalInt("1");
		}
		return res;
		/*
		string x,y;
		x=fractionPart.get_s();
		reverse(x.begin(),x.end());
		y=other.fractionPart.get_s();
		reverse(y.begin(),y.end());
		BigDecimalInt a,b,c;
		a.set_s(x);
		b.set_s(y);
		c=a+b;
		x=c.get_s();

		if(c.size()>max(a.size(),b.size()))
		{
			res.intPart = res.intPart + BigDecimalInt("1");
			x.erase(c.size()-1,1);
		}
		reverse(x.begin(),x.end());
		res.fractionPart.set_s(x);
		return res;
		*/
	}
	BigReal BigReal::operator= (BigReal other)
	{
		intPart=other.intPart;
		fractionPart=other.fractionPart;
		return *this;
	}

	ostream& operator << (ostream& out, BigReal num)
	{

		if( num.intPart.get_s().size()==0)
		{
			out<<"0";
		}
		else
		{
			out<< num.intPart.get_s();
		}
		out << "." ;
		if(num.fractionPart.get_s().size()==0)
		{
			out<<"0";
		}
		else
		{
			out<< num.fractionPart.get_s();
		}

		return out;
	}

	BigReal::~BigReal(){}


int main() {
	BigReal n1 ("11.9000000000000000000000000000000001");
	BigReal n2 ("2333333333339.1134322222222292");
	BigReal n3 = n1 + n2;
	cout << n3<<endl;
	n3 = n3 + 00.9;
	cout << n3<<endl;
	int x;
	string s1,s2;
	bool k=1;
	while (k)
	{
		cout<<"enter the first number\n";
		cin>>s1;
		cout<<"enter the second number\n";
		cin>>s2;
		BigReal num1(s1),num2(s2);
		while(1)
		{
			cout<<"1)for addition\n2)exit\n";
			cin>>x;
			if (x==1)
			{
				cout<<num1+num2<<endl;
				break;
			}
			else if(x==2)
			{
				k=false;
				break;
			}
		}
	}
	return 0;
}

